angular.module('Clairebot').service('_Message', function($log){
	this.person = person;
	this.company = company;
	this.boss = boss;
	this.duration = duration;
	this.responses = [{
		'response_id': 1,
		'bot':true,
		'contents': [{
			'type': 'string',
			'content': 'Hey ' + this.person + '!'
		}]
	},
	{
		'response_id': 2,
		'bot':true,
		'contents': [{
			'type': 'string',
			'content': 'This is Claire here. I have recently joined ' + this.company + ' as '+this.boss+'\'s new Culture assisant. '
		}, {
			'type': 'string',
			'content': 'Congratulations on completing x months! On this occasion today, I’m here to have a word with you about your last month in ' + this.company + '.'
		}]
	},
	{
		'response_id': 3,
		'bot':true,
		'contents': [{
			'type': 'string',
			'content': 'I\'ll be sending you messages, and you can respond by clicking the buttons that appear. Let\'s begin?'
		},
		{
			'type': 'button',
			'content': 'Yes, let\'s begin.',
			'action': 'begin',
			'active': true
		}]
	}];
	this.questions = [{
		'question_id': 1,
		'score': [{
			'contents': [{
				'type': 'string',
				'content': 'How was your last one month in ' + this.company + '?'
			},
			{
				'type': 'radio',
				'options': [
				{
					'type': 'image',
					'content': 'images/depressed.svg',
					'action': 'depressed'
				},
				{
					'type': 'image',
					'content': 'images/sad.svg',
					'action': 'sad'
				},
				{
					'type': 'image',
					'content': 'images/neutral.svg',
					'action': 'neutral'
				},
				{
					'type': 'image',
					'content': 'images/happy.svg',
					'action': 'happy'
				},
				{
					'type': 'image',
					'content': 'images/loving.svg',
					'action': 'loving'
				}
				]
			}
			]
		}]
	},
	{
		'question_id': 2,
		'score': [{
			'contents': [{
				'type': 'string',
				'content': 'Oh...my! Sorry to hear that ' + this.person + '.'
			},
			{
				'type': 'string',
				'content': 'What\'s been working well for you and your team?'
			}]
		},
		{
			'contents': [{
				'type': 'string',
				'content': 'Not so good, I see.'
			},
			{
				'type': 'string',
				'content': 'What\'s been working well for you and your team?'
			}
			]
		},
		{
			'contents': [{
				'type': 'string',
				'content': 'Alright, I\'ll note that down.'
			},
			{
				'type': 'string',
				'content': 'What\'s been working well for you and your team?'
			}]
		},
		{
			'contents': [{
				'type': 'string',
				'content': 'Wow! Sounds good.'
			},
			{
				'type': 'string',
				'content': 'What\'s been working well for you and your team?'
			}]
		},
		{
			'contents': [{
				'type': 'string',
				'content': 'That\'s brilliant!'
			},
			{
				'type': 'string',
				'content': 'What\'s been working well for you and your team?'
			}]
		}
		]
	},
	{
		'question_id': 3,
		'score': [{
			'contents': [{
				'type': 'string',
				'content': 'Just one last question ' + this.person + '.'
			},
			{
				'type': 'string',
				'content': 'If we were to re-live the last one month, what are some areas that could have improved?'
			}]
		}]
	},
	{
		'question_id': 4,
		'score': [{
			'contents': [{
				'type': 'string',
				'content': 'All right. Thanks for sharing ' + this.person + '. I’ll be sending my report to ' + this.boss + '. In case. you’d like to edit your answer you can scroll up and do that.'
			},
			{
				'type': 'button',
				'action': 'submit',
				'content': 'All good. Send to ' + this.boss + '!',
				'active': true
			}]
		}]
	},
	{
		'question_id': 5,
		'score': [{
			'contents': [{
				'type': 'string',
				'content': 'Done! I will touch base with you again in a month to see if things have changed. Bye!'
			}]
		}]
	}
	];
	this.matches = [{
		'response_id': 3,
		'question_id': 1,
		'matches': [
		{
			'term': 'yes',
			'score': 0
		},
		{
			'term': 'yep',
			'score': 0
		},
		{
			'term': 'let\'s begin',
			'score': 0
		},
		{
			'term': 'let\'s start',
			'score': 0
		},
		{
			'term': 'start',
			'score': 0
		},
		{
			'term': 'yes, let\'s start',
			'score': 0
		},
		{
			'term': 'Yes, let\'s begin.',
			'score': 0
		},
		{
			'term': 'Yeah sure.',
			'score': 0
		}]
	},
	{
		'response_id': 4,
		'question_id': 2,
		'matches': [
		{
			'term': 'sad',
			'score': 0
		},
		{
			'term': 'very sad',
			'score': 0
		},
		{
			'term': 'Sad',
			'score': 0
		},
		{
			'term': 'depressed',
			'score': 0
		},
		{
			'term': 'unhappy',
			'score': 1
		},
		{
			'term': 'Unhappy',
			'score': 1
		},
		{
			'term': 'bored',
			'score': 1
		},
		{
			'term': 'bad',
			'score': 1
		},
		{
			'term': 'neutral',
			'score': 2
		},
		{
			'term': 'okay',
			'score': 2
		},
		{
			'term': 'not so bad',
			'score': 2
		},
		{
			'term': 'good',
			'score': 3
		},
		{
			'term': 'happy',
			'score': 3
		},
		{
			'term': 'superb',
			'score': 4
		},
		{
			'term': 'very happy',
			'score': 4
		},
		{
			'term': 'great',
			'score': 4
		}]
	},
	{
		'response_id': 5,
		'question_id': 3,
		'score': 0
	},
	{
		'response_id': 6,
		'question_id': 4,
		'score': 0
	},
	{
		'response_id': 7,
		'question_id': 5,
		'matches': [{
			'term': 'submit',
			'score': 0
		},
		{
			'term': 'all good',
			'score': 0
		},
		{
			'term': 'send',
			'score': 0
		},
		{
			'term': 'All good. Send it to ' + this.boss + '.',
			'score': 0
		}]
	}
	];
	this.anonResponses = [{
		'response_id': 1,
		'bot':true,
		'contents': [{
			'type': 'string',
			'content': 'Hi there! You can anonymously share your concerns with ' + this.boss + ' over here. ' + this.boss + ' may get back to you here (he won\'t find your identity) after seeing your message.'
		}]
	},
	{
		'response_id': 2,
		'bot':true,
		'contents': [{
			'type': 'string',
			'content': 'This is a place for you to share things you feel might be harming the business or the culture.'
		}]
	}]
	this.updatePersonAttributes = function(person, company)
	{
		this.person = person;
		this.company = company;
	}
	this.getResponse = function(response_id)
	{
		var responseObj;
		this.responses.forEach(function(response){
			if(response.response_id === response_id)
			{
				responseObj = response;
				return false;
			}
		});
		return responseObj;
	}
	this.getAnonResponse = function(response_id)
	{
		var responseObj;
		this.anonResponses.forEach(function(response){
			if(response.response_id === response_id)
			{
				responseObj = response;
				return false;
			}
		});
		return responseObj;
	}
	this.processMessage = function(message, lastResponse, lastBotResponse)
	{
		var self = this;
		var match = [];
		var expected = this.matches[lastBotResponse - 3];
		if(expected.response_id == lastBotResponse)
		{
			if(expected.hasOwnProperty('matches'))
			{
				expected.matches.forEach(function(matchTest){
					var approximity = self.compare(message, matchTest.term);
					approximityCheck = approximity > 0.7 ? true : false;
					if(approximityCheck)
					{
						console.log(message + " has matched " + matchTest.term + " at " + approximity);
						matchTest.question_id = expected.question_id;
						match.push(matchTest);
					}
					return approximityCheck;
				});
			}
			else
			{
				match.push(expected);
			}
		}
		if(lastBotResponse < 4)
		{
			var response = {
				'response_id': lastResponse + 1,
				'bot': false,
				'contents': [
				{
					'type': 'string',
					'content': message
				}],
				'hidePencil': true
			};
		}
		else
		{
			var response = {
				'response_id': lastResponse + 1,
				'bot': false,
				'contents': [
				{
					'type': 'string',
					'content': message
				}
				]
			};
		}
		self.responses.push(response);
		if(match.length)
		{
			console.log("matched next obj", match);
			self.questions.forEach(function(question){
				if(match[0].question_id == question.question_id)
				{
					console.log("found question", question);
					var response = {
						'response_id': lastResponse + 2,
						'bot': true,
						'contents': question.score[match[0].score].contents
					}
					self.responses.push(response);
					return false;
				}
			});
		}
	}
	this.checkNewResponses = function(lastResponse) {
		var newResponses = [];
		this.responses.forEach(function(response){
			if(response.response_id > lastResponse)
			{
				newResponses.push(response);
			}
		});
		return newResponses;
	}
	this.compare = function(s1,s2){
		var self = this;
		var longer = s1;
		var shorter = s2;
		if (s1.length < s2.length) {
			longer = s2;
			shorter = s1;
		}
		var longerLength = longer.length;
		if (longerLength == 0) {
			return 1.0;
		}
		return (longerLength - self.editDistance(longer, shorter)) / parseFloat(longerLength);
	}
	this.editDistance = function(s1, s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();

		var costs = new Array();
		for (var i = 0; i <= s1.length; i++) {
			var lastValue = i;
			for (var j = 0; j <= s2.length; j++) {
				if (i == 0)
					costs[j] = j;
				else {
					if (j > 0) {
						var newValue = costs[j - 1];
						if (s1.charAt(i - 1) != s2.charAt(j - 1))
							newValue = Math.min(Math.min(newValue, lastValue),
								costs[j]) + 1;
						costs[j - 1] = lastValue;
						lastValue = newValue;
					}
				}
			}
			if (i > 0)
				costs[s2.length] = lastValue;
		}
		return costs[s2.length];
	}
	this.endResponse = function(lastResponse) {
		var self = this;
		var response = {
			'response_id': lastResponse + 1,
			'bot': true,
			'contents': [{
				'type': 'string',
				'content': 'One more thing ' + this.person + '. In case you want to anonymously share something with ' + this.boss + ', you can have a chat with Anonymous Bat on the left.'
			}]
		};
		self.responses.push(response);
		console.log('Responses:', self.responses);
	}
});