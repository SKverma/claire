var searchTerm = window.location.search.split('?');
var queries = typeof(searchTerm[1]) != 'undefined' ? searchTerm[1].split('&') : ['Rachel', 'Wayne Enterprises', 'Bruce', '1'];
var person = queries[0];
var boss = queries[2];
var company = queries[1];
var duration = queries[3];

angular.module('Clairebot', ['ngMaterial', 'ngRoute', 'ui.router', 'luegg.directives'])
	.controller('ChatMsgCtrl', function ($log, $rootScope, $scope, $mdDialog, _Message, $location, $timeout, $interval) {
		$scope.hideloader = false;
		$scope.botchatdone = false;
		$scope.anonymouschatdone = false;
		$scope.btndiabled = false;
		$timeout(function () { $scope.hideloader = true; }, 3000);
		$scope.glued = true;
		var runningIntervals = [];
		$scope.botChat = function() {
			$scope.hidechatloader = false;
			$scope.btndiabled = false;		
			$timeout(function () { $scope.hidechatloader = true; }, 4000);
			runningIntervals.forEach(function(interval){
				$interval.cancel(interval);
			});
			$scope.anonChatMessage = false;
			$scope.msgs = [];
			$scope.lastResponse = 0;
			$scope.lastBotResponse = 0;
	    	$scope.msgs.push(_Message.getResponse(1));
			$scope.lastResponse++;
			$scope.lastBotResponse++;
	    	$scope.msgs.push(_Message.getResponse(2));
			$scope.lastResponse++;
			$scope.lastBotResponse++;
	    	$timeout(function(){
	    		$scope.msgs.push({'loader': true, 'bot': true});
	    	}, 500);
	    	$timeout(function(){
	    		$scope.msgs.pop();
	    		$timeout(function(){
	    			$scope.msgs.push(_Message.getResponse(3));
					$scope.lastResponse++;
					$scope.lastBotResponse++;
	    		}, 800);
	    	}, 1500);
	    	$timeout(function(){
		    	var intervalPromise = $interval(function(){
		    		if($scope.lastBotResponse == 8)
		    		{
		    			_Message.endResponse($scope.lastResponse);
		    		}
		    		if($scope.lastBotResponse == 9)
		    		{
		    			$interval.cancel(intervalPromise);
						$scope.botchatdone = true;
		    		}
		    		var newResponses = _Message.checkNewResponses($scope.lastResponse);
		    		console.log('new Responses', newResponses);
		    		if(newResponses)
		    		{
		    			newResponses.forEach(function(response){
		    				$scope.msgs.push(response);
		    				if(response.bot)
		    				{
								$scope.lastBotResponse++;
		    				}
		    				$scope.lastResponse++;
		    			});
		    		}
		    	}, 1000);
	    		runningIntervals.push(intervalPromise);
	    	}, 3000);
	        console.log('data incoming', $scope.msgs);
		}
		$scope.anonChat = function() {
			$scope.hidechatloader = false;
			$scope.anonymouschatdone = false;
			$scope.botchatdone = false;		
			$timeout(function () { $scope.hidechatloader = true; }, 2000);
			$scope.msgs = [];
	    	$scope.msgs.push(_Message.getAnonResponse(1));
	    	$scope.msgs.push(_Message.getAnonResponse(2));
	    	$scope.anonChatMessage = true;
	        console.log('data incoming', $scope.msgs);
		}
        $scope.sendMessage = function (form) {
            console.log('data to save', form);
            if(!$scope.anonChatMessage)
            {
            	var getMessage = _Message.processMessage(form.message, $scope.lastResponse, $scope.lastBotResponse);
            }
            else
            {
				var response = {
					'response_id': 3,
					'bot': false,
					'contents': [{
						'type': 'string',
						'content': form.message
					}]
				}
				var confirm = $mdDialog.confirm()
						.title('Important Message')
						.textContent('Are you sure you want to send your message to Bruce?')												
						.ok('Yes')
						.cancel('Neah! Leave it');
						$mdDialog.show(confirm).then(function () {
							$scope.msgs.push(response);
							$timeout(function(){
	            			var response = {
								'response_id': 4,
								'bot': true,
								'contents': [{
									'type': 'string',
									'content': $scope.getAnonBatResponses()
								}]
							}
							$scope.msgs.push(response);
						}, 1000);
						}, function () {
							$scope.status = 'You decided to keep your debt.';
						});		
            }
            form.message = '';
        }
        $scope.editMessage = function (response) {
        	response.editing = false;
            console.log('show edit', response);
            var count = 0;
            $scope.msgs.forEach(function(message){
            	if(message.response_id = response.response_id)
            	{
            		$scope.msgs[count].message = response;
            	}
            	count++;
            });
        }
        $scope.responseButtonAction = function (action) {
        	switch(action) {
        		case 'begin':
        			_Message.processMessage("Yes, let's begin.", $scope.lastResponse, $scope.lastBotResponse);
					$scope.btndiabled = true;
        			break;
        		case 'depressed':
        			_Message.processMessage("Sad", $scope.lastResponse, $scope.lastBotResponse);
					$scope.disbalebtns = true;
        			break;
        		case 'sad':
        			_Message.processMessage("Unhappy", $scope.lastResponse, $scope.lastBotResponse);
					$scope.disbalebtns = true;
        			break;
        		case 'neutral':
        			_Message.processMessage("Okay", $scope.lastResponse, $scope.lastBotResponse);
					$scope.disbalebtns = true;
        			break;
        		case 'happy':
        			_Message.processMessage("Good", $scope.lastResponse, $scope.lastBotResponse);
					$scope.disbalebtns = true;
        			break;
        		case 'loving':
        			_Message.processMessage("Great", $scope.lastResponse, $scope.lastBotResponse);
					$scope.disbalebtns = true;
        			break;
        		case 'submit':        								
					_Message.processMessage("All good. Send it to " + boss + ".", $scope.lastResponse, $scope.lastBotResponse);					
        			break;					
        	}
        }
		$scope.botChat();

        $scope.selectChat = function(arg){
        	if(arg == 'anonChat')
        	{
        		$scope.anonChat();
        	}
        	else
        	{
        		$scope.botChat();
        	}
        }
		$scope.autoExpand = function (e) {
			var element = typeof e === 'object' ? e.target : document.getElementById(e);
			var scrollHeight = element.scrollHeight - 25; // replace 60 by the sum of padding-top and padding-bottom
			element.style.height = scrollHeight + "px";
		};
		$scope.getAnonBatResponses = function(){
			var responses = [
			'Thanks ' + person + '! I\'ve sent your message to ' + boss + '. I\'ll let you know over email when the response comes.',
			'I\'ve noted it down, '+ person + '! I\'ll send it over to ' + boss + '. You\'ll be notified over email when ' + boss + ' responds.',
			'I hear you ' + person + '. I\'ll inform ' + boss + ' about this. Will share an email update once he replies back.'
			]
			$scope.anonymouschatdone = true;
			return responses[Math.floor(Math.random() * 2)];			
		}
    });